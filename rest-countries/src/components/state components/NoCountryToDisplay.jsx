import React from "react"
import "./NoCountryToLoad.css"

const NoCountryToDisplay = () => {
    return (
        <div className="no-country-ui">
            <div className="no-country"></div>
            <div className="no-country-text">
                ⚠️ Couldn't find the country you are looking for...
            </div>
        </div>
    )
}

export default NoCountryToDisplay
