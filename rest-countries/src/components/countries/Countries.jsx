import React, { useState, useEffect, useContext } from "react"
import Filter from "./Filter"
import "./Countries.css"
import { ThemeContext } from "../../App"
import { useNavigate } from "react-router-dom"
import UnableToLoad from "../state components/UnableToLoad"
import NoCountryToDisplay from "../state components/NoCountryToDisplay"
import Loader from "../state components/Loader"

const REST_COUNTRIES_URL = `https://restcountries.com/v3.1/all`

const Countries = () => {
    const [countries, setCountries] = useState([])
    const [search, setSearch] = useState("")
    const [region, setRegion] = useState("")
    const [subRegion, setSubRegion] = useState("")
    const [responseOk, setResponseOk] = useState(true)
    const [isLoading, setIsLoading] = useState(true)
    const [sortBy, setSortBy] = useState("")
    const [sortOrder, setSortOrder] = useState(false)

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(REST_COUNTRIES_URL)
                if (!response.ok) {
                    throw new Error("Failed to fetch data")
                }
                const countriesData = await response.json()
                setCountries(countriesData)
                setIsLoading(false)
            } catch (error) {
                console.error("Error fetching data:", error)
                setResponseOk(false)
                setIsLoading(false)
            }
        }
        fetchData()
    }, [])

    const darkTheme = useContext(ThemeContext)

    const countryCardStyles = {
        backgroundColor: darkTheme ? "#2b3743" : "#ffffff",
        color: darkTheme ? "#ffffff" : "#000000",
        boxShadow: darkTheme ? "5px 5px 10px #1f2c35" : "5px 5px 10px #f2f2f2",
    }

    const countryRegions = countries.reduce((acc, curr) => {
        if (!acc.includes(curr.region)) {
            acc.push(curr.region)
        }
        return acc
    }, [])

    const countrySubRegions = countries.reduce((acc, curr) => {
        const region = curr.region
        const subregion = curr.subregion

        if (!acc[region]) {
            acc[region] = []
        }

        if (!acc[region].includes(subregion)) {
            acc[region].push(subregion)
        }

        return acc
    }, {})

    const filteredData = countries
        // REGION FILTER
        .filter((value) => {
            return region.toLowerCase() === ""
                ? value
                : value.region.toLowerCase().includes(region.toLowerCase())
        })
        // SUB-REGION FILTER
        .filter((value) => {
            return subRegion.toLowerCase() === ""
                ? value
                : value.subregion
                      .toLowerCase()
                      .includes(subRegion.toLowerCase())
        })
        // COUNTRY FILTER
        .filter((value) => {
            return search.toLowerCase() === ""
                ? value
                : value.name.common.toLowerCase().includes(search.toLowerCase())
        })

    const sortedData = () => {
        let sortedCountries = [...filteredData]
        if (sortBy === "area") {
            sortedCountries.sort((country1, country2) =>
                sortOrder === true
                    ? country1.area - country2.area
                    : country2.area - country1.area
            )
        } else if (sortBy === "population") {
            sortedCountries.sort((country1, country2) =>
                sortOrder === true
                    ? country1.population - country2.population
                    : country2.population - country1.population
            )
        }
        return sortedCountries
    }

    const navigate = useNavigate()

    const countriesDisplay = () => (
        <div className="countries">
            {sortedData().map((value) => {
                return (
                    <div className="country-data" key={value.name.common}>
                        <div
                            className="country-data-inner"
                            style={countryCardStyles}
                            onClick={() =>
                                navigate(
                                    `/country/${value.name.common.toLowerCase()}`
                                )
                            }
                        >
                            <div className="country-flag">
                                <img
                                    src={value.flags.png}
                                    alt={value.flags.alt}
                                    className="country-flag-img"
                                />
                            </div>
                            <div className="country-name">
                                {value.name.common}
                            </div>
                            <div className="country-population">
                                <div>Population:&nbsp;</div>
                                {value.population}
                            </div>
                            <div className="country-region">
                                <div>Region:&nbsp;</div> {value.region}
                            </div>
                            <div className="country-capital">
                                <div>Capital:&nbsp;</div> {value.capital}
                            </div>
                        </div>
                    </div>
                )
            })}
        </div>
    )

    return (
        <div className="countries-container">
            <Filter
                setSearch={setSearch}
                setRegion={setRegion}
                setSubRegion={setSubRegion}
                regionsData={countryRegions}
                subRegionsData={countrySubRegions}
                setSortBy={setSortBy}
                setSortOrder={setSortOrder}
            />
            {isLoading ? (
                <Loader />
            ) : responseOk ? (
                filteredData.length == 0 ? (
                    <NoCountryToDisplay />
                ) : (
                    countriesDisplay()
                )
            ) : (
                <UnableToLoad />
            )}
        </div>
    )
}

export default Countries
