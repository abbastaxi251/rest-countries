import React, { useState, useContext } from "react"
import "./Filter.css"
import SearchButton from "../../assets/search.svg"
import { ThemeContext } from "../../App"

const Filter = (props) => {
    const {
        setRegion,
        setSearch,
        setSubRegion,
        regionsData,
        subRegionsData,
        setSortBy,
        setSortOrder,
    } = props

    const [regionSelected, setRegionSelected] = useState("")

    const darkTheme = useContext(ThemeContext)

    const filterContainerStyle = {
        backgroundColor: darkTheme ? "#202d36" : "#fafafa",
        color: darkTheme ? "#ffffff" : "#000000",
    }

    const filterStyles = {
        backgroundColor: darkTheme ? "#2b3743" : "#fafafa",
        color: darkTheme ? "#ffffff" : "#000000",
        boxShadow: darkTheme ? "5px 5px 10px #1f2c35" : "5px 5px 10px #f2f2f2",
    }

    const filterStylesById = {
        backgroundColor: darkTheme ? "#2b3743" : "#fafafa",
        color: darkTheme ? "#ffffff" : "#000000",
    }

    const SearchCountry = () => {
        const inputClassName = darkTheme
            ? "dark-theme-input"
            : "light-theme-input"

        return (
            <div className="search-country" style={filterStyles}>
                <form style={filterStyles}>
                    <img src={SearchButton} alt="" />
                    <input
                        type="text"
                        placeholder=" Search for a country..."
                        name="search-countries"
                        id="search-countries"
                        style={filterStylesById}
                        className={inputClassName}
                        onChange={(event) => {
                            setSearch(event.target.value)
                        }}
                    />
                </form>
            </div>
        )
    }

    const FilterByRegion = () => (
        <div className="filter-by-region" style={filterStyles}>
            <label for="region-filter"></label>
            <select
                id="region-filter"
                name="region-filter"
                style={filterStylesById}
                onChange={(event) => {
                    setRegion(event.target.value)
                    setSubRegion("")
                    setRegionSelected(event.target.value)
                }}
            >
                <option value="" selected>
                    Filter by Region - All
                </option>
                {regionsData.map((region) => {
                    return (
                        <option value={region} key={region}>
                            {region}
                        </option>
                    )
                })}
            </select>
        </div>
    )

    const FilterBySubRegion = () => (
        <div className="filter-by-sub-region" style={filterStyles}>
            <label for="sub-region-filter"></label>
            <select
                id="sub-region-filter"
                name="sub-region-filter"
                style={filterStylesById}
                onChange={(event) => {
                    setSubRegion(event.target.value)
                }}
            >
                <option value="" selected>
                    Filter by Sub Region - All
                </option>
                {subRegionsData[regionSelected] &&
                    subRegionsData[regionSelected].map((subRegion) => {
                        return (
                            <option value={subRegion} key={subRegion}>
                                {subRegion}
                            </option>
                        )
                    })}
            </select>
        </div>
    )

    const SortByContainer = () => (
        <div className="sort-by-container" style={filterStyles}>
            <label for="sort-by"></label>
            <select
                id="sort-by"
                name="sort-by"
                style={filterStylesById}
                onChange={(event) => {
                    setSortBy(event.target.value)
                }}
            >
                <option value="none" selected>
                    Sort by - None
                </option>
                <option value="area">Area</option>
                <option value="population">Population</option>
            </select>
            <div
                className="sort-order"
                onClick={() => {
                    setSortOrder((value) => !value)
                }}
            >
                ⇅
            </div>
        </div>
    )

    return (
        <div className="filter-container" style={filterContainerStyle}>
            {SearchCountry()}
            {FilterByRegion()}
            {FilterBySubRegion()}
            {SortByContainer()}
        </div>
    )
}

export default Filter
