import React, { useState } from "react"
import "./App.css"
import Countries from "./components/countries/Countries"
import Header from "./components/header/Header"
import { Routes, Route } from "react-router-dom"
import CountryDetails from "./components/country details/CountryDetails"

export const ThemeContext = React.createContext()

function App() {
    const [darkTheme, setDarkTheme] = useState(false)

    const toggleTheme = () => {
        setDarkTheme((prevTheme) => !prevTheme)
    }

    const themeStyles = {
        backgroundColor: darkTheme ? "#202d36" : "#fafafa",
        color: darkTheme ? "white" : "black",
    }

    return (
        <div className={"countries-container"} style={themeStyles}>
            <ThemeContext.Provider value={darkTheme}>
                <Header toggleTheme={toggleTheme} />
                <Routes>
                    <Route path="/" element={<Countries />} />
                    <Route
                        path="/country/:countryName"
                        element={<CountryDetails />}
                    />
                </Routes>
            </ThemeContext.Provider>
        </div>
    )
}

export default App
