import React, { useContext } from "react"
import "./Header.css"
import { ThemeContext } from "../../App"

const Header = (props) => {
    const darkTheme = useContext(ThemeContext)
    const { toggleTheme } = props

    const themeStyles = {
        backgroundColor: darkTheme ? "#2b3743" : "#ffffff",
        color: darkTheme ? "#ffffff" : "#000000",
        boxShadoe: darkTheme
            ? "0px 5px 5px 5px #1f2c35"
            : "0px 5px 5px 5px #f0f0f0",
    }

    return (
        <header style={themeStyles}>
            <div className="header-title">Where in the world?</div>
            <div className="theme">
                <button
                    className="theme-text"
                    style={themeStyles}
                    onClick={toggleTheme}
                >
                    ☾ Dark Mode
                </button>
            </div>
        </header>
    )
}

export default Header
