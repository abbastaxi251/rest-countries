import React, { useEffect, useState, useContext } from "react"
import { useNavigate, useParams } from "react-router-dom"
import "./CountryDetails.css"
import { ThemeContext } from "../../App"

const REST_COUNTRIES_URL = `https://restcountries.com/v3.1/name`

const CountriesDetails = () => {
    const { countryName } = useParams()

    const [countryData, setCountryData] = useState([])

    const [responseOk, setResponseOk] = useState(true)
    const [isLoading, setIsLoading] = useState(true)

    const navigate = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(
                    `${REST_COUNTRIES_URL}/${countryName}?fullText=true`
                )
                if (!response.ok) {
                    throw new Error("Failed to fetch data")
                }
                const data = await response.json()
                setCountryData(data[0])
                setIsLoading(false)
            } catch (error) {
                console.error("Error fetching data:", error)
                setResponseOk(false)
                setIsLoading(false)
            }
        }
        fetchData()
    }, [])

    const darkTheme = useContext(ThemeContext)

    const containerStyle = {
        backgroundColor: darkTheme ? "#212e37" : "#fafafa",
        color: darkTheme ? "#ffffff" : "#000000",
    }

    const buttonStyle = {
        backgroundColor: darkTheme ? "#2b3743" : "#ffffff",
        color: darkTheme ? "#ffffff" : "#000000",
        boxShadow: darkTheme ? "5px 5px 10px #1f2c35" : "5px 5px 10px #f2f2f2",
    }

    const imgStyle = {
        border: darkTheme ? "20px solid #1f2c35" : "20px solid #fefefe",
    }

    const countryDisplay = () => (
        <div className="countries-details-container" style={containerStyle}>
            <div className="go-back">
                <button onClick={() => navigate(-1)} style={buttonStyle}>
                    ⬅ Back
                </button>
            </div>
            <div className="country-container">
                <div className="country-flag-2">
                    <img
                        style={imgStyle}
                        src={countryData.flags.png}
                        alt={countryData.flags.alt}
                        className="country-flag-image"
                    />
                </div>
                <div className="country-data">
                    <div className="country-name-2">
                        {countryData.name.common}
                    </div>
                    <div className="more-info">
                        <div className="native-name">
                            <div>Native Name:&nbsp;</div>
                            {Object.values(countryData.name.nativeName).map(
                                (name, index) => (
                                    <span key={index}>
                                        {`${name.common} `}&nbsp;
                                    </span>
                                )
                            )}
                        </div>
                        <div className="population">
                            <div>Population:&nbsp;</div>
                            {countryData.population}
                        </div>
                        <div className="region-info">
                            <div>Region:&nbsp;</div>
                            {countryData.region}
                        </div>
                        <div className="subregion-info">
                            <div>Sub Region:&nbsp;</div>
                            {countryData.subregion}
                        </div>
                        <div className="capital-info">
                            <div>Capital:&nbsp;</div>
                            {countryData.capital}
                        </div>
                        <div className="currency-info">
                            <div>Currency:&nbsp;</div>
                            {Object.keys(countryData.currencies) + " "}
                        </div>
                        <div className="languages-info">
                            <div>Languages:&nbsp;</div>
                            {Object.values(countryData.languages) + " "}
                        </div>
                    </div>
                    <div className="border-countries">
                        <div>Border Countries:&nbsp;</div>
                        {countryData.borders != null &&
                            Object.values(countryData.borders).map((item) => {
                                return (
                                    <div>
                                        <button style={buttonStyle}>
                                            {item}
                                        </button>
                                        &nbsp;
                                    </div>
                                )
                            })}
                    </div>
                </div>
            </div>
        </div>
    )

    return isLoading ? <div>Loading</div> : countryDisplay()
}

export default CountriesDetails
