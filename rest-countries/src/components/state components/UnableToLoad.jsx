import React from "react"
import "./UnableToLoad.css"

const UnableToLoad = () => {
    return (
        <div className="error-ui">
            <div className="error"></div>
            <div className="error-text">
                ⚠️ Unable to fetch countries data...
            </div>
        </div>
    )
}

export default UnableToLoad
